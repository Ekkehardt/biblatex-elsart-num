biblatex-elsart-num
===================

Numeric Citation- and Bibliography style for Elsevier journals
--------------------------------------------------------------


Systems
-------
independent

Tested with TeX Live 2016 in Ubuntu 16.04.


Licence
-------
The biblatex-Style 'elsart-num' is licenced under LPPL 1.3.
